/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import loginPage from './src/screen/loginPage';
import navigationScreen from './src/navigation/navigationScreen';
import jobListPage from './src/screen/jobListPage';

AppRegistry.registerComponent(appName, () => navigationScreen);
