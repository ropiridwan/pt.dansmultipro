import React, {useEffect, useState} from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Switch,
} from 'react-native';
import axios from 'axios';
import Ionicons from 'react-native-vector-icons/Ionicons';

const jobListPage = ({navigation}) => {
  const [dataJob, setDataJob] = React.useState([]);
  const [openFilter, setOpenFilter] = React.useState(false);
  const [textSearch, setTextSearch] = React.useState('');
  const [textLocation, setTextLocation] = React.useState('');
  const [applyFilter, setApplyFilter] = React.useState(false);
  const [dataFilter, setDataFilter] = React.useState([]);

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  useEffect(() => {
    axios
      .get('http://dev3.dansmultipro.co.id/api/recruitment/positions.json')
      .then(response => {
        setDataJob(response.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const filterSearch = (keywordSearch) => {
    setTextSearch(keywordSearch)
    const datajob = dataJob.filter(function (item) {
        return item.company == keywordSearch
      });
      setDataFilter(datajob);

  }
  const filterJobs = () => {
    const datajob = dataJob.filter(function (item) {
      if (isEnabled && textLocation) {
        return item.location == textLocation && item.type == 'Full Time';
      } else if (isEnabled) {
        return item.type == 'Full Time';
      } else {
        return item.location == textLocation;
      }
    });
    setApplyFilter(true);
    setDataFilter(datajob);
  };
  const dataJobFunct = () => {
    return dataJob.map(data => {
      return (
        <TouchableOpacity
          onPress={() => navigation.navigate('Job Detail', data)}
          style={Styles.containerJob}>
          <Image
            style={{
              width: 50,
              height: 50,
            }}
            source={{uri: data.company_logo}}
            resizeMode="contain"
          />
          <View
            style={{
              paddingRight: 20,
              width: '80%',
            }}>
            <Text numberOfLines={2} style={Styles.textJob}>
              {data.company}
            </Text>
            <Text numberOfLines={2} style={Styles.textJob}>
              {data.title}
            </Text>
            <Text style={Styles.textJob}>{data.location}</Text>
          </View>
          <Ionicons
            style={Styles.iconJobStyle}
            name="chevron-forward-outline"
          />
        </TouchableOpacity>
      );
    });
  };

  const dataJobFilter = () => {
    return dataFilter.map(data => {
      return (
        <>
          <TouchableOpacity
            onPress={() => navigation.navigate('Job Detail', data)}
            style={Styles.containerJob}>
            <Image
              style={{
                width: 50,
                height: 50,
              }}
              source={{uri: data.company_logo}}
              resizeMode="contain"
            />
            <View
              style={{
                paddingRight: 20,
                width: '80%',
              }}>
              <Text numberOfLines={2} style={Styles.textJob}>
                {data.company}
              </Text>
              <Text numberOfLines={2} style={Styles.textJob}>
                {data.title}
              </Text>
              <Text style={Styles.textJob}>{data.location}</Text>
            </View>
            <Ionicons
              style={Styles.iconJobStyle}
              name="chevron-forward-outline"
            />
          </TouchableOpacity>
        </>
      );
    });
  };

  const toggleOpenFilter = () => {
    setOpenFilter(!openFilter);
  };

  return (
    <View style={Styles.container}>
      <View style={{flexDirection: 'row', marginBottom: 20, marginTop: 10}}>
        <View
          style={{
            borderWidth: 1,
            borderColor: 'black',
            borderRadius: 16,
            marginLeft: 16,
            // padding: 10,
            flexDirection: 'row',
            width:'80%',
            height:40
          }}>
          <Ionicons
            style={[Styles.iconStyle, {alignSelf: 'center'}]}
            name="search"
          />
          <TextInput
            style={{flex: 1, color: 'black'}}
            onChangeText={keywordSearch => {filterSearch(keywordSearch)}}
            // value={textSearch}
          />
        </View>
        <TouchableOpacity onPress={() => toggleOpenFilter()}>
          <Ionicons style={Styles.iconStyle} name="chevron-down-outline" />
        </TouchableOpacity>
      </View>
      {openFilter && (
        <View style={[Styles.containerJobFilter]}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={Styles.textRegular}>Fulltime</Text>
            <Switch
              trackColor={{false: 'white', true: 'green'}}
              thumbColor={isEnabled ? 'white' :'black'}
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={Styles.textRegular}>Location</Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: 'black',
                width: '50%',
                height: 40,
                color: 'black',
                padding: 10,
              }}
              onChangeText={setTextLocation}
              value={textLocation}
            />
          </View>
          <TouchableOpacity
            onPress={filterJobs}
            style={{
              backgroundColor: 'gray',
              borderWidth: 1,
              borderColor: 'black',
              padding: 4,
              alignSelf: 'flex-end',
              width: '30%',
              height: 30,
              marginVertical: 10,
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <Text style={[Styles.textRegular, {marginVertical: 0}]}>
              Apply Filter
            </Text>
          </TouchableOpacity>
        </View>
      )}
      <ScrollView>
        {(applyFilter && dataFilter[0]?.location !== undefined) ||
        (applyFilter && isEnabled) || textSearch ? (
          <View>
            <Text style={[Styles.textBold, {marginLeft: 10}]}>
              Search Result
            </Text>
            {dataJobFilter()}
          </View>
        ) : (
          dataJobFunct()
        )}
      </ScrollView>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  iconStyle: {
    fontSize: 25,
    // margin:10,
    marginHorizontal: 20,
    color: 'black',
  },
  iconJobStyle: {
    fontSize: 25,
    // margin:10,
    marginLeft: 8,
    color: 'black',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textJob: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 8,
  },
  containerTouchable: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    width: '80%',
    marginLeft: 16,
    height: 40,
  },
  containerJob: {
    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: 16,
    width: '90%',
    // height:40,
    alignSelf: 'center',
    marginVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  containerJobFilter: {
    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: 16,
    width: '90%',
    // height:40,
    marginVertical: 8,
    paddingHorizontal: 8,
  },
});
export default jobListPage;
