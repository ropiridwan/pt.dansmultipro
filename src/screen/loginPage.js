import React from 'react';
import {
  TextInput,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
} from 'react-native';

const loginPage = ({navigation}) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  const loginButton = () => {
    navigation.navigate('Job List');
  };

  const dataDisabled = email === '' && password === '' ? true : false;
  return (
    <View style={Styles.container}>
      <Text style={Styles.textBold}>USER LOGIN</Text>
      <Text style={Styles.textRegular}>Email</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setEmail}
          style={{color: 'black', padding: 10}}
          value={email}
          placeholder="Masukan Email"
        />
      </View>
      <Text style={Styles.textRegular}>Password</Text>
      <View style={Styles.containerTouchable}>
        <TextInput
          onChangeText={setPassword}
          style={{color: 'black', padding: 10}}
          value={password}
          placeholder="Masukan Password"
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity
        disabled={dataDisabled}
        onPress={loginButton}
        style={[
          Styles.containerTouchable,
          {
            backgroundColor: dataDisabled ? 'gray' : '#87CEEB',
            marginTop: 20,
            paddingVertical: 8,
          },
        ]}>
        <Text
          style={[Styles.textBold, {alignSelf: 'center', marginVertical: 0}]}>
          Login
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
  },
  containerTouchable: {
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    width: 150,
    height: 40,
  },
});

export default loginPage;
