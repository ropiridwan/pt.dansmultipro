import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Linking,
  useWindowDimensions,
} from 'react-native';
import RenderHtml from 'react-native-render-html';

const jobDescription = props => {
  const {width} = useWindowDimensions();

  const [dataJob, setDataJob] = React.useState([]);
  const {
    company_logo,
    title,
    company,
    company_url,
    description,
    created_at,
    location,
    type,
    url,
  } = props.route.params;

  const dataJobFunct = () => {
    return (
      <View style={{marginTop: 20}}>
        <Text style={Styles.textBold}>Job Specification</Text>
        <View style={Styles.containerJobDesc}>
          <Text
            style={[
              Styles.textRegular,
              {color: 'gray', marginTop: 10, marginBottom: 4},
            ]}>
            Title
          </Text>
          <Text style={Styles.textRegular}>{title}</Text>
          <Text
            style={[
              Styles.textRegular,
              {color: 'gray', marginTop: 10, marginBottom: 4},
            ]}>
            Fulltime
          </Text>
          <Text style={Styles.textRegular}>
            {type == 'Full Time' ? 'Yes' : 'No'}
          </Text>
          <Text
            style={[
              Styles.textRegular,
              {color: 'gray', marginTop: 10, marginBottom: 4},
            ]}>
            Description
          </Text>
          <RenderHtml
            baseStyle={Styles.textRegular}
            source={{html: description}}
          />
        </View>
      </View>
    );
    // });
  };

  return (
    <View style={Styles.container}>
      <Text style={Styles.textBold}>Company</Text>
      <View
        style={Styles.containerJob}>
        <Image
          style={{
            width: 50,
            height: 50,
          }}
          source={{uri: company_logo}}
          resizeMode="contain"
        />
        <View
          style={{
            paddingRight: 20,
            width: '100%',
          }}>
          <Text numberOfLines={2} style={Styles.textJob}>
            {company}
          </Text>
          <Text numberOfLines={2} style={Styles.textJob}>
            {title}
          </Text>
          <TouchableOpacity
            style={{width: '20%'}}
            onPress={() => {
              Linking.openURL(company_url);
            }}>
            <Text
              style={[Styles.textJob, {color: 'blue', borderBottomWidth: 1}]}>
              Go To Website
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>{dataJobFunct()}</ScrollView>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  iconStyle: {
    fontSize: 25,
    // margin:10,
    marginHorizontal: 20,
    color: 'black',
  },
  iconJobStyle: {
    fontSize: 25,
    // margin:10,
    marginLeft: 8,
    color: 'black',
  },
  textBold: {
    fontSize: 16,
    fontWeight: '700',
    color: 'black',
    marginVertical: 10,
    marginLeft: 16,
  },
  textRegular: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
  },
  textJob: {
    fontSize: 12,
    fontWeight: '700',
    color: 'black',
    marginVertical: 8,
  },
  containerTouchable: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 16,
    width: '80%',
    marginLeft: 16,
    height: 40,
  },
  containerJob: {
    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: 16,
    borderRadius: 8,
    width: '90%',
    // height:40,
    alignSelf: 'center',
    marginVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  containerJobDesc: {
    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: 16,
    width: '90%',
    borderRadius: 8,
    // height:40,
    alignSelf: 'center',
    marginVertical: 8,
    paddingHorizontal: 8,
  },
});
export default jobDescription;
