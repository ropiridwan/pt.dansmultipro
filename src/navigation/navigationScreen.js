import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import loginPage from '../screen/loginPage';
import jobListPage from '../screen/jobListPage';
import jobDescription from '../screen/jobDescription';

const Stack = createNativeStackNavigator();

const navigationScreen = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="loginPage">
        <Stack.Screen
          name="loginPage"
          component={loginPage}
          options={{
            title: 'Login',
            // headerStyle: {backgroundColor: '#87CEEB'},
            headerTitleStyle: {
              color: 'black',
            },
            headerTitleAlign: 'center',
          }}
        />
        <Stack.Screen
            name='Job List'
            component={jobListPage} 
            options={{ title:'Job List',
                // headerStyle:{backgroundColor:'#87CEEB'},
                 headerTitleStyle:{
                        color:'black',
                },
                  headerBackTitleStyle:{
                        color:'black'
                },
                 headerTitleAlign:'center'
                 }}/>
        <Stack.Screen
            name='Job Detail'
            component={jobDescription} 
            options={{ title:'Job Detail',
                // headerStyle:{backgroundColor:'#87CEEB'},
                 headerTitleStyle:{
                        color:'black',
                },
                  headerBackTitleStyle:{
                        color:'black'
                },
                 headerTitleAlign:'center'
                 }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default navigationScreen;
